$(document).ready(function () {




    $('.btn-read-more').click(function(){
        $('.about-content').toggleClass('hide');
        if ($('.about-content').hasClass('hide')) {
            $('.btn-read-more').html('Читать далее');
        } else {
            $('.btn-read-more').html('Скрыть');
        }
    });

    $(".accordion-item-title").on("click",function(){
        $(this).toggleClass("open");
        $(this).next(".accordion-item-content").stop();
        $(this).next(".accordion-item-content").slideToggle();
    })

    $(".accordion-item-btn-1").on("click",function(){
        $(".accordion-item-map-1").slideToggle();
    })
    $(".accordion-item-btn-2").on("click",function(){
        $(".accordion-item-map-2").slideToggle();
    })
    $(".accordion-item-btn-3").on("click",function(){
        $(".accordion-item-map-3").slideToggle();
    })
  
   
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
          $this.html(event.strftime(''
              + '<div class="countdown"><span class="countdown__number">%D</span><span class="countdown__title">Дней</span></div>'
              + '<div class="countdown"><span class="countdown__number">%H</span><span class="countdown__title">Часов</span></div>'
              + '<div class="countdown"><span class="countdown__number">%M</span><span class="countdown__title">Минут</span></div>'
              + '<div class="countdown"><span class="countdown__number">%S</span><span class="countdown__title">Секунд</span></div>'));
        });
    });

    
    $(".tabs-caption").on("click", ".tab-button:not(.active)", function() {
        $(this)
            .addClass("active")
            .siblings()
            .removeClass("active")
            .closest(".tabs")
            .find(".tabs-content")
            .removeClass("active")
            .eq($(this).index())
            .addClass("active");
    });

});

